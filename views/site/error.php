<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;//colocamos el titulo a la vista con el nombre del error y el codigo
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) //colocamos el mensaje de error?>
    </div>

    <p>
       Upss...
    </p>
    <p>
        Hay un problema
    </p>

</div>
