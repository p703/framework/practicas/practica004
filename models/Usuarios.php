<?php
namespace app\models;

use Yii;
use yii\base\Model;



class Usuarios extends Model{
    public $nombre;
    public $apellidos;
    public $edad;
    public $email;
    public function rules() {
        return[
            [['nombre','edad','email'],'required','messaje'=>'{attribute} es obligatorio'],
            ['email','email','messaje'=>'Escribir correctamente el correo electrónico'],
            ['edad','number','min'=>18,'max'=>65,'tooBig'=>'La edad debe estar entre 18 y 65','tooSmall'=>'La edad debe estar entre 18 y 65']
        ];
    }
    
    public function attributeLabels(){
        return [
            'nombre'=>'Nombre de usuario',
        ];
    }
    
}

